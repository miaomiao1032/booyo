#booyo

> booyo是一套基于mongodb、zookeeper、hbase、activemq、solr构建的分布式采集系统，目前还在开发中。
> 原型请查看doc/原型设计/booyo

##模块

###调度
负责总控

###爬虫
基于高匿代理的大规模集群爬取，

###存储
同步存储到mongodb和hbase

###解析
基于模板解析文本，提取结构化数据

###导出
导出到mysql、mongodb、solr、elsticsearch、hbase
