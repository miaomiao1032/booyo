package cn.ultrarisk.booyo.spider.http;

import cn.ultrarisk.booyo.core.spider.Proxy;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * http请求对象
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/18
 * @since V1.0
 */
public class Request implements Serializable {
    private static final long serialVersionUID = 5405585401085330672L;

    public static Request DEFAULT = Builder.build();
    /**
     * 请求类型
     */
    private Method method;
    /**
     * 内容类型，包括文件类型和网页编码
     */
    private String contentType;
    /**
     * 超时时间
     */
    private int timeout;
    /**
     * 请求头
     */
    private Map<String, String> headers;
    /**
     * cookie
     */
    private Map<String, String> cookies;
    /**
     * 请求参数
     */
    private Map<String, String[]> params;
    /**
     * 代理
     */
    private Proxy proxy;

    public Method getMethod() {
        return method;
    }

    public String getContentType() {
        return contentType;
    }

    public int getTimeout() {
        return timeout;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public Map<String, String[]> getParams() {
        return params;
    }

    public Proxy getProxy() {
        return proxy;
    }

    public static class Builder {
        public static Request build() {
            return null;
        }
    }

    /**
     * RequestMethod
     *
     * @author sevendlong(lin23871@163.com)
     * @date 2014/9/24
     * @since V1.0
     */
    public static enum Method {
        GET("GET", "httpGet请求"),
        POST("POST", "httpPost请求");

        private String code;
        private String remark;

        Method(String code, String remark) {
            this.code = code;
            this.remark = remark;
        }

        public String getCode() {
            return code;
        }

        public String getRemark() {
            return remark;
        }

        public static Map<String, String> methods() {
            Map<String, String> methods = new HashMap<String, String>();
            for (Method method : Method.values()) {
                methods.put(method.code, method.remark);
            }
            return methods;
        }
    }
}
