package cn.ultrarisk.booyo.spider.exception;

/**
 * BooyoSpiderException
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/18
 * @since V1.0
 */
public class BooyoSpiderException extends RuntimeException {
    private static final long serialVersionUID = -6390785016181491044L;

    public BooyoSpiderException() {
    }

    public BooyoSpiderException(String message) {
        super(message);
    }

    public BooyoSpiderException(String message, Throwable cause) {
        super(message, cause);
    }

    public BooyoSpiderException(Throwable cause) {
        super(cause);
    }
}
