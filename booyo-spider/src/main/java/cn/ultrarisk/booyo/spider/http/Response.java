package cn.ultrarisk.booyo.spider.http;

import cn.ultrarisk.booyo.core.spider.Page;
import cn.ultrarisk.booyo.core.util.IOUtil;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Http响应对象
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/18
 * @since V1.0
 */
public class Response {
    private int statusCode;
    private String contentType;
    private Map<String, String> headers;
    private InputStream content;

    public Response(CloseableHttpResponse response) throws IOException {
        this.statusCode = response.getStatusLine().getStatusCode();
        this.contentType = response.getEntity().getContentType().getValue();
        this.headers = new HashMap<String, String>();
        for (Header header : response.getAllHeaders()) {
            this.headers.put(header.getName(), header.getValue());
        }
        this.content = new ByteArrayInputStream(IOUtil.in2Bytes(response.getEntity().getContent()));
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getContentType() {
        return contentType;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public InputStream getContent() {
        return content;
    }

    public static class Converter {
        public static Page convert() {
            return null;
        }
    }
}
