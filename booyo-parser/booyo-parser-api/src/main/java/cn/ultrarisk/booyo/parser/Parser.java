package cn.ultrarisk.booyo.parser;

/**
 * Parser
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/13
 * @since V1.0
 */
public interface Parser {
    public void parse(String source, String rule);
}
