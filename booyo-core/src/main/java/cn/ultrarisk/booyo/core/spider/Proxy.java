package cn.ultrarisk.booyo.core.spider;

import cn.ultrarisk.booyo.core.util.StringUtil;

import java.io.Serializable;

/**
 * 代理对象
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2014/9/18
 * @since V1.0
 */
public class Proxy implements Serializable {
    private static final long serialVersionUID = 2060850270787405656L;

    /**
     * 代理ip或域名
     */
    private String host;
    /**
     * 端口
     */
    private int port;
    /**
     * 代理类型
     */
    private Type type;
    /**
     * 代理协议
     */
    private Protocol protocol;
    private String username;
    private String password;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAuthEnabled() {
        return StringUtil.isNotEmpty(this.username) && StringUtil.isNotEmpty(this.password);
    }

    /**
     * 代理协议
     */
    public static enum Protocol {
        HTTP("http"), HTTPS("https");

        private String value;
        Protocol(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * 代理类型
     */
    public static enum Type {
        COMMON(1, "普通"), ANON(2, "高匿");

        private int code;
        private String name;

        Type(int code, String name) {
            this.code = code;
            this.name = name;
        }

        public int getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }
}
