package cn.ultrarisk.booyo.core.util;

import org.springframework.util.StringUtils;

import java.util.regex.Pattern;

/**
 * StringUtil
 *
 * @author sevendlong(lin23871@163.com)
 * @date 2013-11-6
 * @since V1.0
 */
public class StringUtil extends StringUtils {
    /**
     * 是否为空
     *
     * @param s
     * @return
     */
    public static boolean isEmpty(String s) {
        return (s == null || "".equals(s.trim()));
    }

    /**
     * 是否不为空
     *
     * @param s
     * @return
     */
    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }

    /**
     * 是否包含中文
     *
     * @param s
     * @return
     */
    public static boolean isContainZH(String s) {
        return isEmpty(s) || Pattern.compile("[\\u4e00-\\u9fa5]").matcher(s).find();
    }

    public static String trim(String s) {
        return s != null ? s.trim() : null;
    }
}
