package cn.ultrarisk.booyo.core.exception;

/**
 * BooyoCoreException
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/16
 * @since V1.0
 */
public class BooyoCoreException extends RuntimeException {
    private static final long serialVersionUID = 8687974396353510525L;

    public BooyoCoreException() {
    }

    public BooyoCoreException(String message) {
        super(message);
    }

    public BooyoCoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public BooyoCoreException(Throwable cause) {
        super(cause);
    }
}
