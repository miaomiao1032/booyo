package cn.ultrarisk.booyo.storage.entry;

import cn.ultrarisk.booyo.storage.entry.config.ApplicationConfig;
import cn.ultrarisk.booyo.storage.exception.BooyoStorageException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Main
 *
 * @author longlin(lin23871@163.com)
 * @date 2014/9/16
 * @since V1.0
 */
public class Main {
    public static void main(String[] args) {
        if (args == null || args.length == 0) {
            throw new BooyoStorageException("the args was wrong");
        }
        String config = args[0];

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
        }
    }
}
